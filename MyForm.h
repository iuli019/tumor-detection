#pragma once
#include "ImageProcessing.h"
using namespace cv;
using namespace std;

ImageProcessing imagePre("C:\\Users\\iulia\\Desktop\\ProcessedImages\\radiografiePre.jpeg");
ImageProcessing imagePost("C:\\Users\\iulia\\Desktop\\ProcessedImages\\radiografiePost.jpg");

int areaPre;
int areaPost;
namespace TumorDetection {


	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Windows;
	using namespace std;


	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
		}
		

	protected:
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^ pictureBox1;
	protected:
	private: System::Windows::Forms::PictureBox^ pictureBox2;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::Button^ button4;




	private: System::Windows::Forms::Button^ button9;
	private: System::Windows::Forms::Button^ button5;
	private: System::Windows::Forms::Button^ button6;
	private: System::Windows::Forms::Button^ button7;
	private: System::Windows::Forms::Button^ button8;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::OpenFileDialog^ openFileDialog1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Label^ label6;
	private: System::Windows::Forms::Label^ label7;

	

	private:
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button9 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->button8 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(263, 174);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(380, 589);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// pictureBox2
			// 
			this->pictureBox2->Location = System::Drawing::Point(863, 174);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(380, 589);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 1;
			this->pictureBox2->TabStop = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(1316, 294);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(155, 49);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Upload Image";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(1316, 375);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(155, 49);
			this->button2->TabIndex = 3;
			this->button2->Text = L"Median Filter";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(1316, 459);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(155, 49);
			this->button3->TabIndex = 4;
			this->button3->Text = L"K-Means Clustering";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(1316, 540);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(155, 49);
			this->button4->TabIndex = 5;
			this->button4->Text = L"Calculate Area";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// button9
			// 
			this->button9->Location = System::Drawing::Point(672, 783);
			this->button9->Name = L"button9";
			this->button9->Size = System::Drawing::Size(155, 49);
			this->button9->TabIndex = 10;
			this->button9->Text = L"Compare";
			this->button9->UseVisualStyleBackColor = true;
			this->button9->Click += gcnew System::EventHandler(this, &MyForm::button9_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(48, 540);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(155, 49);
			this->button5->TabIndex = 14;
			this->button5->Text = L"Calculate Area";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &MyForm::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(48, 459);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(155, 49);
			this->button6->TabIndex = 13;
			this->button6->Text = L"K-Means Clustering";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &MyForm::button6_Click);
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(48, 375);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(155, 49);
			this->button7->TabIndex = 12;
			this->button7->Text = L"Median Filter";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &MyForm::button7_Click);
			// 
			// button8
			// 
			this->button8->Location = System::Drawing::Point(48, 294);
			this->button8->Name = L"button8";
			this->button8->Size = System::Drawing::Size(155, 49);
			this->button8->TabIndex = 11;
			this->button8->Text = L"Upload Image";
			this->button8->UseVisualStyleBackColor = true;
			this->button8->Click += gcnew System::EventHandler(this, &MyForm::button8_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Times New Roman", 28.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(478, 23);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(573, 55);
			this->label1->TabIndex = 15;
			this->label1->Text = L"Tumor Detection Application";
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"C:";
			this->openFileDialog1->Filter = L"JPEG Files (*jpg)|*.jpg|PNG Files (*.png)|*.png";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(260, 783);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(0, 21);
			this->label2->TabIndex = 16;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(860, 783);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(0, 21);
			this->label3->TabIndex = 17;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(660, 407);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(0, 17);
			this->label4->TabIndex = 18;
			this->label4->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Times New Roman", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(950, 113);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(205, 34);
			this->label5->TabIndex = 19;
			this->label5->Text = L"After Treatment";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Font = (gcnew System::Drawing::Font(L"Times New Roman", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label6->Location = System::Drawing::Point(335, 113);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(224, 34);
			this->label6->TabIndex = 20;
			this->label6->Text = L"Before Treatment";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(660, 424);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(0, 17);
			this->label7->TabIndex = 21;
			this->label7->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1521, 862);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button8);
			this->Controls->Add(this->button9);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}

#pragma endregion
		//upload buttons
	private: System::Void MyForm_Load(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		pictureBox2->ImageLocation = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\radiografiePost.jpg";
	}
	private: System::Void button8_Click(System::Object^ sender, System::EventArgs^ e) {
		pictureBox1->ImageLocation = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\radiografiePre.jpeg";
	}
// medianFilter Buttons
private: System::Void button7_Click(System::Object^ sender, System::EventArgs^ e) {
	imagePre.medianFilter();
	pictureBox1->ImageLocation = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\filteredImage.jpg";
}
private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {
	imagePost.medianFilter();
	pictureBox2->ImageLocation = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\filteredImage.jpg";
}
	   //k-means buttons
private: System::Void button6_Click(System::Object^ sender, System::EventArgs^ e) {
	imagePre.K_Means();
	pictureBox1->ImageLocation = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\clusteredImage.jpg";
}
private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	imagePost.K_Means();
	pictureBox2->ImageLocation = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\clusteredImage.jpg";
}

	   // calculate area buttons
private: System::Void button5_Click(System::Object^ sender, System::EventArgs^ e) {
	
	areaPre = imagePre.countPixels();
	
	label2->Text = "Area in pixels: " + imagePre.countPixels() + " Image size in pixels: " + imagePre.getClusteredImage().total();
}
private: System::Void button4_Click(System::Object^ sender, System::EventArgs^ e) {
	
	areaPost = imagePost.countPixels();
	
	label3->Text = "Area in pixels: " + imagePost.countPixels() + " Image size in pixels: " + imagePost.getClusteredImage().total();
}

	   // compare button
private: System::Void button9_Click(System::Object^ sender, System::EventArgs^ e) {

	if (areaPre > areaPost) {
		label4->Text = "Tumor has shrunk in size.";
		label7->Text = "Treatment was successful.";
	}
	else
	{
		label4->Text = "Tumor has increased in size.";
		label7->Text = "Treatment was not successful.";
	}
	
}
};
}
