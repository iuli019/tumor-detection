#pragma once
#ifndef IMAGE_H
#define IMAGE_H

#include<iostream>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include <opencv2/core/types_c.h>

using namespace std;
using namespace cv;

class ImageProcessing {
private:
	string pathToImage;
	Mat filteredImage;
	Mat clusteredImage;
	vector<vector<Point>> contours;

public:
	ImageProcessing(string path);

	void medianFilter();
	void insertionSort(int window[]);
	void K_Means();
	int countPixels();

	//getters 
	Mat getClusteredImage();
};
#endif