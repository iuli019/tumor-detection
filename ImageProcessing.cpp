#include "ImageProcessing.h"

ImageProcessing::ImageProcessing(string path) {
    this->pathToImage = path;
};

void ImageProcessing::medianFilter() {
    Mat src, dst;
    
    // citim imaginea din path si o facem alb negru
    string image_path = samples::findFile(this->pathToImage);
    src = imread(image_path, IMREAD_GRAYSCALE);
    
    // verificam daca e empty
    if (src.empty())
    {
        cout << "Could not read the image: " << image_path << endl;
        exit(1);
    }
    
    int window[9];

    //initializam dimensiunile unei matrici noi de aceeasi dimensiune cu cea a imaginii initiale
    dst = src.clone();
    for (int y = 0; y < src.rows; y++)
        for (int x = 0; x < src.cols; x++)
            dst.at<uchar>(y, x) = 0.0;


    for (int y = 1; y < src.rows - 1; y++) {
        for (int x = 1; x < src.cols - 1; x++) {
            //se memoreaza valoarea pentru pixelul curent si vecinii acestuia
            window[0] = src.at<uchar>(y - 1, x - 1);
            window[1] = src.at<uchar>(y, x - 1);
            window[2] = src.at<uchar>(y + 1, x - 1);
            window[3] = src.at<uchar>(y - 1, x);
            window[4] = src.at<uchar>(y, x);
            window[5] = src.at<uchar>(y + 1, x);
            window[6] = src.at<uchar>(y - 1, x + 1);
            window[7] = src.at<uchar>(y, x + 1);
            window[8] = src.at<uchar>(y + 1, x + 1);

            // se sorteaza valorile memorate
            this->insertionSort(window);

            //se salveaza valoarea mediana
            dst.at<uchar>(y, x) = window[4];
        }
    }
  
    string path = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\filteredImage.jpg";
    imwrite(path, dst);
    this->filteredImage = dst;
}

void ImageProcessing::insertionSort(int window[])
{
    int temp, i, j;
    for (i = 0; i < 9; i++) {
        temp = window[i];
        for (j = i - 1; j >= 0 && temp < window[j]; j--) {
            window[j + 1] = window[j];
        }
        window[j + 1] = temp;
    }
}

void ImageProcessing::K_Means() {
    // convertim valorile matricei in float
    Mat samples(filteredImage.rows * filteredImage.cols, filteredImage.channels(), CV_32F); 

   for (int y = 0; y < filteredImage.rows; y++)
        for (int x = 0; x < filteredImage.cols; x++)
            // transformam fiecare pixel din matricea filteredImage in usinged char
            //si  il asignam pixelului coresp din matricea sample
            samples.at<float>(y + x * filteredImage.rows, 0) = filteredImage.at<uchar>(y, x);
                
    Mat labels, centers;
    int attempts = 5;
    int K = 2; // numarul clusterelor
    kmeans(samples, K, labels,
        TermCriteria(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 10000, 0.0001), 
        attempts, KMEANS_PP_CENTERS, centers);

    // intializam o matrice pentru noua imagine, cu aceleasi dimensiuni ca imaginea filtrata
    //si cu acelasi tip de elemente
    Mat new_image(filteredImage.size(), filteredImage.type());

    //refacem matricea
    for (int y = 0; y < filteredImage.rows; y++)
        for (int x = 0; x < filteredImage.cols; x++)
        {
            int cluster_idx = labels.at<int>(y + x * filteredImage.rows, 0);
            new_image.at<uchar>(y, x) = centers.at<float>(cluster_idx, 0);
        }
    
    for (int i = 0; i < new_image.rows; i++) // transformam imaginea intr-una binara
        for (int j = 0; j < new_image.cols; j++)
            if (new_image.at<uchar>(i, j) >= 50)
                new_image.at<uchar>(i, j) = 255;
            else
                new_image.at<uchar>(i, j) = 0;

    this->clusteredImage = new_image;
    string path = "C:\\Users\\iulia\\Desktop\\ProcessedImages\\clusteredImage.jpg";
    imwrite(path, new_image);
}

int ImageProcessing::countPixels() {
    Mat src = clusteredImage;
   
   int whitePixels = countNonZero(src);
   
    return whitePixels;
}

Mat ImageProcessing::getClusteredImage() {
    return this->clusteredImage;
};